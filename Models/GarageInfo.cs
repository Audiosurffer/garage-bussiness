﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageBussines.Models
{
    public class GarageInfo
    {
        public string TestKey { get; set; }
        public bool ShowGarageInfo => !string.IsNullOrEmpty(TestKey);
    }
}
